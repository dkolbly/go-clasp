# clasp

This is a command-line tool for interfacing with the Google Script
API.  It provides similar (though at this point a subset) of the
functionality of Google's `clasp` tool (hence our name), but is more
friendly to my own use cases.

## Origins

This tool started from Google's own Go quickstart for the Script API.
It has evolved substantially from there.

## Configuration

You need to have _credentials_ and a _token_ to use this tool.

The credentials essentially define this as an app in your Google
Compute Platform project.  They can be obtained by enabling the "Apps
Script API" in GCP and getting JSON credentials from there.  It can be
shared with everyone on your project.

The token identifies _your user_ as having authorized this app to
access the script API on your behalf.  It should not be shared.

By default, go-clasp stores these in the `~/.config/go-clasp/`
directory.  It has no way to create the credentials, but if you are
lacking a token when you run the tool, it will prompt you to visit a
particular URL which will start the OAuth dance.  Once complete, you
will get a code that can be pasted back in to this tool.

## Operation

### Compatibility Mode

If you do not supply an `--id` argument, either on the command line or
in the APPS_SCRIPT_ID environment variable, then this script guesses
you might be in Google clasp compatibility mode and reads information
from the `.clasp.json` file.

### Pushing Code

```
clasp push [--id SCRIPT] [--dir DIR]
```

### Creating a Version

```
clasp version [--id SCRIPT] LABEL
```

### List Versions

```
clasp versions
```

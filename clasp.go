package main

import (
	"crypto/sha256"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/dkolbly/cli/v3"
	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/script/v1"
)

var app = &cli.App{
	Name:    "clasp",
	Usage:   "manage Google Apps Scripts",
	Version: "v0.3.0",
	Commands: []*cli.Command{
		loginCommand,
		showCommand,
		pushCommand,
		versionsCommand,
		versionCommand,
	},
}

var loginCommand = &cli.Command{
	Name:   "login",
	Usage:  "log in to Google Apps API",
	Action: doLogin,
	Flags: []cli.Flag{
		&cli.BoolFlag{
			Name:    "global",
			Aliases: []string{"g"},
			Usage:   "store login token in ~/.config/go-clasp",
		},
	},
}

var showCommand = &cli.Command{
	Name:   "show",
	Usage:  "show information about a script",
	Action: doShow,
	Flags: []cli.Flag{
		idFlag,
	},
}

var pushCommand = &cli.Command{
	Name:   "push",
	Usage:  "update an Apps Script project on GCP",
	Action: doPush,
	Flags: []cli.Flag{
		idFlag,
		&cli.StringFlag{
			Name:  "dir",
			Usage: "directory to push",
		},
		&cli.BoolFlag{
			Name:    "dry-run",
			Aliases: []string{"n"},
			Usage:   "only report what we *would* do",
		},
	},
}

var versionCommand = &cli.Command{
	Name:   "version",
	Usage:  "create a named script version",
	Action: doVersion,
	Flags: []cli.Flag{
		idFlag,
	},
}

var versionsCommand = &cli.Command{
	Name:   "versions",
	Usage:  "list script versions",
	Action: doVersions,
	Flags: []cli.Flag{
		idFlag,
	},
}

var idFlag = &cli.StringFlag{
	Name:    "script",
	Aliases: []string{"s"},
	EnvVars: []string{"APPS_SCRIPT_ID"},
	Usage:   "the Script ID of script project",
}

func main() {
	if app.Run(os.Args) != nil {
		os.Exit(1)
	}
}

var service *script.Service
var scriptID string
var rootDir string

var scriptIdMissing = errors.New(`Script ID has not been specified; either supply
a '--script' (-s) flag, an APPS_SCRIPT_ID environment variable, or
have a '.clasp.json' file here.)
`)

var needToLogIn = errors.New(`No user token found; run 'clasp login' to log in to the
Apps Script service
`)

func setupCredentials() (*oauth2.Config, error) {
	b, err := configData("CLASP_APP_CREDENTIALS", "credentials.json")
	if err != nil {
		log.Fatalf("Unable to read client secret file: %v", err)
		return nil, err
	}
	// If modifying these scopes, delete your previously saved token.json.
	config, err := google.ConfigFromJSON(b,
		script.ScriptProjectsScope,
		script.ScriptDeploymentsScope,
	)
	if err != nil {
		log.Fatalf("Unable to parse client secret file to config: %v", err)
		return nil, err
	}
	return config, nil
}

func setup(c *cli.Context) error {
	config, err := setupCredentials()
	if err != nil {
		return err
	}

	if !c.IsSet("script") {
		// compatibility mode... see if we can read .clasp.json
		buf, err := ioutil.ReadFile(".clasp.json")
		if err != nil {
			return scriptIdMissing
		}

		var tmp struct {
			ScriptID string `json:"scriptId"`
			RootDir  string `json:"rootDir"`
		}
		err = json.Unmarshal(buf, &tmp)
		if err != nil {
			return err
		}
		scriptID = tmp.ScriptID
		rootDir = tmp.RootDir
	} else {
		scriptID = c.String("script")
	}

	client, err := getClient(config)
	if err != nil {
		return err
	}

	srv, err := script.New(client)
	if err != nil {
		log.Fatalf("Unable to retrieve Script client: %v", err)
		return err
	}
	service = srv

	return nil
}

func doVersions(c *cli.Context) error {
	err := setup(c)
	if err != nil {
		return err
	}
	listVersions(service, scriptID)
	return nil
}

func doVersion(c *cli.Context) error {
	err := setup(c)
	if err != nil {
		return err
	}
	name := c.Args().First()
	makeVersion(service, scriptID, name)
	return nil
}

func doPush(c *cli.Context) error {
	err := setup(c)
	if err != nil {
		return err
	}

	if c.IsSet("dir") {
		rootDir = c.String("dir")
	}

	con, err := service.Projects.GetContent(scriptID).Do()

	ix := makeFileIndexFromDir(rootDir)
	overlap := make(map[string]bool)
	content := &script.Content{
		ScriptId: scriptID,
	}

	for _, f := range con.Files {
		overlap[f.Name] = true
		local, ok := ix[f.Name]
		if ok {
			if local.Source == f.Source {
				fmt.Printf("  %12s %-14s  ; unchanged\n",
					"("+f.Type+")", f.Name)
			} else {
				var mod string
				if len(f.Source) != len(local.Source) {
					mod = fmt.Sprintf("%d->%d bytes",
						len(f.Source),
						len(local.Source))
				} else {
					mod = fmt.Sprintf("%s->%s hash",
						hashstr(f.Source),
						hashstr(local.Source))
				}
				fmt.Printf("  %12s %-14s  ; modified (%s)\n",
					"("+f.Type+")", f.Name, mod)
			}
		} else {
			fmt.Printf("  %12s %-14s  ; obsolete\n",
				"("+f.Type+")", f.Name)
		}
	}
	for k, f := range ix {
		content.Files = append(content.Files, f)
		if !overlap[k] {
			fmt.Printf("  %12s %-14s  ; new\n",
				"("+f.Type+")", f.Name)
		}
	}
	if !c.Bool("dry-run") {
		t0 := time.Now()
		_, err := service.Projects.UpdateContent(scriptID, content).Do()
		if err != nil {
			// The API encountered a problem.
			log.Fatalf("The API returned an error: %v", err)
		}
		dt := time.Since(t0).Round(time.Millisecond)
		log.Printf("uploaded %d files in %s", len(content.Files), dt)
	}

	return nil
}

func doShow(c *cli.Context) error {
	err := setup(c)
	if err != nil {
		return err
	}

	getRes, err := service.Projects.Get(scriptID).Do()
	if err != nil {
		// The API encountered a problem.
		log.Fatalf("The API returned an error: %v", err)
	}
	fmt.Printf("Title %q\n", getRes.Title)
	fmt.Printf("Last Updated %s\n", getRes.UpdateTime)
	fmt.Printf("\n")
	return nil
}

func configData(env string, s string) ([]byte, error) {
	if env != "" {
		literal := os.Getenv(env)
		if literal != "" {
			return []byte(literal), nil
		}
	}

	// check to see if there's such a file right here
	buf, err := ioutil.ReadFile(s)
	if err == nil {
		return buf, nil
	}

	// otherwise, read it from ~/.config/go-clasp/

	path := fmt.Sprintf("%s/.config/go-clasp/%s", os.Getenv("HOME"), s)
	return ioutil.ReadFile(path)
}

// Retrieve a token, saves the token, then returns the generated client.
func getClient(config *oauth2.Config) (*http.Client, error) {
	// The file token.json stores the user's access and refresh
	// tokens, and is created automatically when the authorization
	// flow completes for the first time.
	var tok *oauth2.Token
	tokData, err := configData("CLASP_TOKEN", "token.json")
	if err != nil {
		return nil, needToLogIn
	}
	tok = new(oauth2.Token)
	err = json.Unmarshal(tokData, tok)
	// the file/data was there but we couldn't parse it :'(
	if err != nil {
		panic(err)
	}
	return config.Client(context.Background(), tok), nil
}

func doLogin(c *cli.Context) error {
	config, err := setupCredentials()
	if err != nil {
		return err
	}
	tok := getTokenFromWeb(config)

	// by default, save it here.  If the --global (-g) flag is specified,
	// we'll save it in the same global place we look at startup, which is
	// ~/.config/go-clasp/token.json
	dst := "token.json"

	if c.Bool("global") {
		dst = filepath.Join(os.Getenv("HOME"), ".config/go-clasp/token.json")
	}
	saveToken(dst, tok)
	return nil
}

// Request a token from the web, then returns the retrieved token.
func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		log.Fatalf("Unable to read authorization code: %v", err)
	}

	tok, err := config.Exchange(context.TODO(), authCode)
	if err != nil {
		log.Fatalf("Unable to retrieve token from web: %v", err)
	}
	return tok
}

// Saves a token to a file path.
func saveToken(path string, token *oauth2.Token) {
	fmt.Printf("Saving credential file to: %s\n", path)
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}

func makeFileIndexFromDir(dir string) map[string]*script.File {
	ix := make(map[string]*script.File)

	filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() || path[0] == '.' {
			return nil
		}
		if strings.HasSuffix(path, "~") {
			return nil
		}

		ext := filepath.Ext(path)

		var ftype string

		switch ext {
		case ".json":
			ftype = "JSON"
		case ".js", ".gs":
			ftype = "SERVER_JS"
		case ".html":
			ftype = "HTML"
		default:
			fmt.Fprintf(os.Stderr, "ignoring %s ; unknown extension\n", path)
			return nil
		}

		data, err := ioutil.ReadFile(path)
		if err != nil {
			log.Fatal(err)
		}
		base := strings.TrimSuffix(filepath.Base(path), ext)
		f := &script.File{
			Name:   base,
			Type:   ftype,
			Source: string(data),
		}
		ix[f.Name] = f
		return nil
	})
	return ix
}

func hashstr(s string) string {
	h := sha256.New()
	h.Write([]byte(s))
	return fmt.Sprintf("%.4x", h.Sum(nil))
}

func listDeployments(srv *script.Service, id string) {
	ds := script.NewProjectsDeploymentsService(srv)
	lst, err := ds.List(id).Do()
	if err != nil {
		panic(err)
	}
	fmt.Printf("%d deployments\n", len(lst.Deployments))
	if lst.NextPageToken != "" {
		fmt.Printf("PLUS SOME -- we don't handle this yet\n")
	}
	for _, d := range lst.Deployments {
		fmt.Printf("ID %q\n", d.DeploymentId)
		fmt.Printf("  Config:\n")
		c := d.DeploymentConfig
		fmt.Printf("    Version: #%d\n", c.VersionNumber)
		fmt.Printf("    Description: %q\n", c.Description)
		fmt.Printf("    ScriptID: %q\n", c.ScriptId)
		fmt.Printf("    ManifestFIle: %q\n", c.ManifestFileName)
	}
}

func listVersions(srv *script.Service, id string) {
	ds := script.NewProjectsVersionsService(srv)
	ctx := context.Background()

	err := ds.List(id).Pages(ctx, func(lst *script.ListVersionsResponse) error {
		for _, v := range lst.Versions {
			fmt.Printf("  #%d: %q\n", v.VersionNumber, v.Description)
		}
		return nil
	})
	if err != nil {
		panic(err)
	}
}

func makeVersion(srv *script.Service, id string, vname string) {
	ds := script.NewProjectsVersionsService(srv)
	v := &script.Version{
		Description: vname,
		ScriptId:    id,
	}

	v1, err := ds.Create(id, v).Do()
	if err != nil {
		panic(err)
	}
	fmt.Printf("#%d\n", v1.VersionNumber)
	fmt.Printf("    Description: %q\n", v1.Description)
	fmt.Printf("    ScriptID: %q\n", v1.ScriptId)
}

module bitbucket.org/dkolbly/go-clasp

require (
	github.com/dkolbly/cli/v3 v3.0.0
	golang.org/x/net v0.0.0-20190301231341-16b79f2e4e95
	golang.org/x/oauth2 v0.0.0-20190226205417-e64efc72b421
	google.golang.org/api v0.1.0
)
